import unittest
from cube import get_volume


class TestCube(unittest.TestCase):
    def test_cube_volume(self):
        length, width, height = 10, 20, 60
        result = get_volume(length, width, height)
        self.assertEqual(result, 12000)


if __name__ == '__main__':
    unittest.main()
import requests

def main():
    response = requests.get("http://api.exchangeratesapi.io/latest")
    if response.status_code != 200:
        print("Status code: ", response.status_code)
        raise Exception('There was an error')
        
    print('Status code:', response.status_code)
    data = response.json()
    print("JSON Data: ", data)
    

if __name__ == '__main__':
    main()
    
# 01 & 02 files are not working because exchangeratesapi requires access key.
# the example in the tutorial is working because access to this endpoint does not require access key
# way back the tutorial is created.
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.http import JsonResponse

from .models import Product, Manufacturer

def product_list(request):
    products = Product.objects.all()
    data = { 'products': list(products.values()) }
    return JsonResponse(data)


def product_detail(request, pk):
    try:
        product = Product.objects.get(pk=pk)
        data = {'product': {
            'name': product.name,
            'manufacturer': product.manafacturer.name,
            'description': product.description,
            'photo': product.photo.url,
            'price': product.price,
            'shipping_cost': product.shipping_cost,
            'quantity': product.quantity,
        }}
        return JsonResponse(data)
    except Product.DoesNotExist:
        response = JsonResponse({
            'error': {
                'code': 404,
                'message': 'product not found',
            }}, 
            status=404)
        return response

def manufacturer_detail(request, pk):
    try: 
        manufacturer = Manufacturer.objects.get(pk=pk)
        data = { 'manufacturer': {
            'name': manufacturer.name,
            'location': manufacturer.location,
            'active': manufacturer.active,
            'products': list(manufacturer.products.values())
        } }
        return JsonResponse(data)
    except Manufacturer.DoesNotExist:
        response = JsonResponse({
            'error': {
            'message': 'manufacturer does not exist',
            'code': '404'
            }},
            status=404
        )
        return response

def active_manufacturers(request):
    try:
        active_manufacturers = Manufacturer.objects.filter(active=True)
        data = {'manufacturers': list(active_manufacturers.values())}
        return JsonResponse(data)
    except Manufacturer.DoesNotExist:
        response = JsonResponse({
            'error': {
            'message': 'manufacturer does not exist',
            'code': '404'
            }},
            status=404
        )
        return response
'''

class ProductDetailView(DetailView):
    model = Product
    template_name = 'products/product_detail.html'


class ProductListView(ListView):
    model = Product
    template_name = 'products/product_list.html'


'''
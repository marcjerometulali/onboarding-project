from itertools import product
from django.urls import path
from .views import active_manufacturers, manufacturer_detail, product_detail, product_list # ProductDetailView, ProductListView

urlpatterns = [
    #path('', ProductListView.as_view(), name='product_list'),
    #path('product/<int:pk>/', ProductDetailView.as_view(), name='product_detail'),
    path('', product_list, name='product_list'),
    path('products/<int:pk>', product_detail, name='product_detail'),
    path('manufacturer/<int:pk>', manufacturer_detail, name='manufacturer_detail'),
    path('active-manufacturers/', active_manufacturers, name='active_manufacturers'),
]

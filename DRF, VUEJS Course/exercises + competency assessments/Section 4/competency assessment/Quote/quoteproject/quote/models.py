from django.db import models
from django.contrib.auth import get_user_model

class Quote(models.Model):
    quote_author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, default=None)
    quote_body = models.TextField()
    context = models.CharField(max_length=250) 
    source = models.CharField(max_length=250)
    created_at = models.DateTimeField(auto_now_add=True)
from typing import List
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from quote.api.serializers import QuoteSerializer
from quote.api.permissions import IsAdminUserorReadOnly
from quote.models import Quote
from quote.api.pagination import SmallSetPagination

class QuoteListCreateAPIView(ListCreateAPIView):
    serializer_class = QuoteSerializer
    queryset = Quote.objects.all()
    permission_classes = [IsAdminUserorReadOnly]
    pagination_class = SmallSetPagination

class QuoteDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = QuoteSerializer
    queryset = Quote.objects.all()
    permission_classes = [IsAdminUserorReadOnly]
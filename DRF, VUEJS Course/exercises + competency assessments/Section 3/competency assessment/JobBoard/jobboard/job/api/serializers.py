from rest_framework import serializers
from job.models import JobOffer

class JobOfferSerializer(serializers.ModelSerializer):
    
    class Meta:
        fields = '__all__'
        model = JobOffer

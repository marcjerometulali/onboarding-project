from django.urls import path
from .views import JobOfferListCreateAPIView, JobOfferDetailView

urlpatterns = [
    path('jobs/', JobOfferListCreateAPIView.as_view(), name='job_offer_list'),
    path('jobs/<int:pk>/', JobOfferDetailView.as_view(), name='job_offer_detail'),
]
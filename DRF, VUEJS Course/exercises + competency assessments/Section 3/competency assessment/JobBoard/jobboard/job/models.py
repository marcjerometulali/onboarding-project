from django.db import models

class JobOffer(models.Model):
    company_name = models.CharField(max_length=250)
    company_email = models.EmailField(null=True, blank=False)
    job_title = models.CharField(max_length=250)
    job_description = models.TextField()
    salary = models.FloatField()
    city = models.CharField(max_length=250)
    state = models.CharField(max_length=250)
    created_at = models.DateField(auto_now_add=True)
    available = models.BooleanField()

const app = Vue.createApp({
    data () {
        return {
            lesson: "Events and Methods",
            counter: 0
        }
    },

    methods: {
        incrementCounter(){
            console.log(this.counter);
            this.counter += 1;
            if(this.counter === 10){
                alert('Counter is at 10');
            }
        }, 
        overTheBox(){
            console.log("Over the green box");
        }
    }
});


const mountedApp = app.mount("#app");
const app = Vue.createApp({
    data() {
        return {
            message: "Hello World!",
            num: 5, 
            img: "https://cdn.pixabay.com/photo/2021/09/05/16/48/rapeseed-6599950_960_720.jpg",
            link: "https://vuejs.org"
        }
    }
});

const mountedApp = app.mount("#app");
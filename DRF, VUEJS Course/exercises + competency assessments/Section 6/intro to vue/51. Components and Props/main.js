let app = Vue.createApp({
    data(){
        return {
            comments: [
                {
                    username: "Alice",
                    content: "First content",
                },
                {
                    username: "Bob",
                    content: "Second content",
                },
                {
                    username: "Peter",
                    content: "Spider",
                },
            ]
        }
    }
});

app.component("comment", {
    props: {
        comment: {
            type: Object,
            required: true,
        }
    },
    template: `
        <div>
            <div class="card-body">
                <strong><p>{{ comment.username }}</p></strong>
                <p>{{ comment.content }}</p>
            </div>
            <hr>
         </div>
    `,
});


let mountedApp = app.mount("#app");
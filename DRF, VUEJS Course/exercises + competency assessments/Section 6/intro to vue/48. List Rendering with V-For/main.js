const app = Vue.createApp({
    data(){
        return {
            users: [
                {
                    id: 567,
                    name: "alice",
                    profession: "developer"
                },
                {
                    id: 567,
                    name: "bob",
                    profession: "manager"
                },
                {
                    id: 567,
                    name: "jane",
                    profession: "singer"
                },
                {
                    id: 567,
                    name: "ben",
                    profession: "hero"
                },
            ],
        }
    },
})

const mountedApp = app.mount("#app");
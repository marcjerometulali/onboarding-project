const app = Vue.createApp({
    data () {
        return {
            'firstName': "John",
            "lastName": "Doe",
        }
    },
    computed: {
        getRandomComputed(){
            return Math.random();
        },

        fullName(){
            return `${this.firstName} ${this.lastName}`;
        },

        reversedFullName(){
            let first = this.firstName.split("").reverse().join("");
            let last = this.lastName.split("").reverse().join("");
            return `${first} ${last}`;
        }

    },
    methods: {
        getRandomNumber(){
            return Math.random();
        }
    },
});

const mountedApp = app.mount("#app");